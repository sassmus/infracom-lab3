package lab3Infracom;




import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Conexion extends Thread {
	protected Socket socket;
	public int contador;
	public int archivo;
	public boolean info = false;
	
	private String checkSumServidor = "";

	private int totalSent = 0;

	long startTime = 0;

	public Conexion(Socket clientSocket, int contador, int archivo) {
		this.socket = clientSocket;
		this.contador = contador;
		this.archivo = archivo;
	}

	public void run() {
		
		Logger logger = Logger.getLogger("Test");  
	    FileHandler fh; 
	    
	    try {  

	        // This block configure the logger with handler and formatter  
	    	
	    	Date date = new Date();  
	        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH_MM_SS");
	        
	        SimpleDateFormat formatterFecha = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
	        
	        fh = new FileHandler( formatter.format(date) + ".log");  
	        logger.addHandler(fh);
	        
	        SimpleFormatter formatter2 = new SimpleFormatter();  
	        fh.setFormatter(formatter2);  

	        // the following statement is used to log any messages  

	        logger.info( this.contador + " Prueba " + formatterFecha.format(date));
	        
	    } catch (SecurityException e) {  
	        e.printStackTrace();  
	    } catch (IOException e) {  
	        e.printStackTrace();  
	    }  
		
		InputStream inp = null;
		BufferedReader brinp = null;
		PrintWriter out = null;
		int tam = 0;
		String ruta = null;
		int a =0;

		try {
			inp = socket.getInputStream();
			brinp = new BufferedReader(new InputStreamReader(inp));
			out = new PrintWriter(socket.getOutputStream(),true);
			out.println("Estado de la conexion: "+socket.isConnected());
			out.flush(); 
		} catch (IOException e) {
			return;
		}
		String line;
		while (true && !socket.isClosed()) {
			try {
				line = brinp.readLine();				
				if(line != null) System.out.println(line);
				if(line!= null) {
					if ( line.equalsIgnoreCase("QUIT")) {
						logger.info( this.contador + " Cerrando socket");
						socket.close();
						return;
					} else {


						if(!info) {
							if(archivo==1){
								//Archivo de 100MiB								
								ruta = "../data/cap2.pptx";

								tam = 92705080;
								out.println("INFO:./data/cap2.pptx,92705080");
								out.flush(); 
							}
							else {
								//Archivo de 250MiB
								ruta = "../data/cap1.pptx";
								tam = 250821787;
								out.println("INFO:./data/cap1.pptx,250821787");
								out.flush(); 
							}
							try {
								logger.info( this.contador + " Ruta del archivo " + ruta);
								logger.info( this.contador + " Tamano del archivo " + tam + " bytes");

								MessageDigest md = MessageDigest.getInstance("SHA-256"); //SHA, MD2, MD5, SHA-256, SHA-384...
								checkSumServidor = checksum(ruta, md);

							} catch (Exception e) {
								e.printStackTrace();
							}

							info = true;
						}
						if(line.equals("Ready to start process"))
						{

							sendFile(ruta, socket, out);
							//Envio de mensaje con el tiempo inicial
							logger.info( this.contador + " Numero de paquetes enviados: " + totalSent);


						}
						else if(line.startsWith("Archivo recibido:")) {
							System.out.println("Recibido");
							String checkSumCliente = line.split("Archivo recibido:")[1];
							logger.info( this.contador + " Checksum recibido del cliente: " + checkSumCliente);
							logger.info( this.contador + " Checksum del servidor: " + checkSumServidor);
							if(checkSumCliente.equals(checkSumServidor)) {
								logger.info( this.contador + " Archivo entregado correctamente.");
							}
						}
						else if(line.startsWith("Tiempo total:")) {
							String tiempo = line.split("Tiempo total:")[1];
							logger.info( this.contador + " Tiempo de envio del archivo: " + tiempo + " segundos");
						}
						else if(line.startsWith("Paquetes:")) {
							String totalRead = line.split("Paquetes:")[1];
							logger.info( this.contador + " Paquetes recibidos: " + totalRead);
						}
						
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}

		}
	}

	public void sendFile(String file, Socket s, PrintWriter out) throws IOException {
		startTime = System.currentTimeMillis();
		System.out.println("TIME:" + startTime);
		out.println("TIME:" + startTime);
		out.flush();


		DataOutputStream dos = new DataOutputStream(s.getOutputStream());
		FileInputStream fis = new FileInputStream(file);
		byte[] buffer = new byte[4096];


		while (fis.read(buffer) > 0) {

			totalSent++;
			dos.write(buffer);
		}

		fis.close();
		//		dos.close();	
	}

	private static String checksum(String filepath, MessageDigest md) throws IOException {

		// file hashing with DigestInputStream
		try (DigestInputStream dis = new DigestInputStream(new FileInputStream(filepath), md)) {
			while (dis.read() != -1) ; //empty loop to clear the data
			md = dis.getMessageDigest();
		}

		// bytes to hex
		StringBuilder result = new StringBuilder();
		for (byte b : md.digest()) {
			result.append(String.format("%02x", b));
		}
		return result.toString();

	}

}