package lab3Infracom;




import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Servidor {

	static final int PORT = 1978;
	public static ArrayList<Socket> listSockets = new ArrayList<>();
	public static BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
	
	public static void main(String args[]) {
		
		
		ServerSocket serverSocket = null;
		Socket socket = null;
		int contador = listSockets.size()+1;

		try {
			serverSocket = new ServerSocket(PORT);
		} catch (IOException e) {
			e.printStackTrace();

		}
		while (contador <=25) {
			
			try {
				socket = serverSocket.accept();
				listSockets.add(socket);
				contador++;
			} catch (IOException e) {
				System.out.println("I/O error: " + e);
			}
		}
		try {
			
			Path currentRelativePath = Paths.get("");
			String s = currentRelativePath.toAbsolutePath().toString();
			System.out.println("Current relative path is: " + s);
			
			System.out.println("Seleccione el archivo que desea descargar \n 1. Archivo de 100MiB \n 2. Archivo de 250MiB");
			int archivo = Integer.parseInt(console.readLine());
			System.out.println("A cuantos clientes desea enviarle el documento de manera simultanea?(1-25)");
			int nClientes = Integer.parseInt(console.readLine());
			System.out.println(nClientes);
			if(nClientes <0 || nClientes>25) throw new Exception("El numero de clienets ingresa no es valido");
			List<Socket> firstNElementsList = listSockets.subList(0, nClientes);
			//ya estan las 25 conexiones
			Iterator<Socket> it = firstNElementsList.iterator();
			int count =0;
			while(it.hasNext())
			{
				Socket actual = it.next();
				new Conexion(actual,count++,archivo).start();

			}
			
			List<Socket> lastElements = listSockets.subList(nClientes, listSockets.size());
			//ya estan las 25 conexiones
			it = lastElements.iterator();
			while(it.hasNext())
			{
				Socket actual = it.next();
				actual.close();

			}
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
